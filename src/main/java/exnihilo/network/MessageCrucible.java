package exnihilo.network;

import io.netty.buffer.ByteBuf;
import cpw.mods.fml.common.network.simpleimpl.IMessage;

public class MessageCrucible implements IMessage {

	public int x, y, z;
	public float fluidVolume, solidVolume;
	
	public MessageCrucible() {}
	
	public MessageCrucible(int x, int y, int z, float fluidVolume, float solidVolume)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.fluidVolume = fluidVolume;
		this.solidVolume = solidVolume;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) 
	{
		this.x = buf.readInt();
		this.y = buf.readInt();
		this.z = buf.readInt();
		this.fluidVolume = buf.readFloat();
		this.solidVolume = buf.readFloat();		
	}

	@Override
	public void toBytes(ByteBuf buf) 
	{
		buf.writeInt(x);
		buf.writeInt(y);
		buf.writeInt(z);
		buf.writeFloat(fluidVolume);
		buf.writeFloat(solidVolume);		
	}
	
	

}
